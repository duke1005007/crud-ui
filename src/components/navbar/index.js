import React from "react";

const NavBar = () => (
  <nav>
    <div className="nav-wrapper grey darken-4">
      <a href="/" className="brand-logo center">
        Тестовое CRUD-UI Звягинцев
      </a>
    </div>
  </nav>
);

export default NavBar;
