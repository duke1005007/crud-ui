import React from "react";

const UserTable = (props) => (
  <table className="responsive-table">
    <thead>
      <tr>
        <th>ID</th>
        <th>Email</th>
        <th>Age</th>
        <th>Login</th>
      </tr>
    </thead>
    <tbody>
      {props.users.map((data) => (
        <tr key={data._id}>
          <td>{data._id}</td>
          <td>{data.data.Email}</td>
          <td>{data.data.Age}</td>
          <td>{data.data.Login}</td>
          <td className="center-align">
            <button
              className="waves-effect waves-light btn-small"
              onClick={() => props.editRow(data)}
            >
              Изменить
            </button>

            <button
              className="waves-effect waves-light btn-small red darken-4"
              onClick={() => props.deleteUser(data._id)}
            >
              Удалить
            </button>
          </td>
        </tr>
      ))}
    </tbody>
  </table>
);

export default UserTable;
