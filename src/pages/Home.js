import React, { Component } from "react";
import api from "../services/api";

import UserTable from "../components/table/UserTable";
import AddUserForm from "../components/forms/AddUserForm";
import EditUserForm from "../components/forms/EditUserForm";

class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      users: [],
      currentUser: {
        Email: "",
        Age: "",
        Login: "",
      },
      editing: false,
    };
  }

  componentDidMount() {
    this.refreshUserTable();
  }

  refreshUserTable() {
    this.usersData = api
      .get("api/records")
      .then((response) => this.setState({ users: response.data }));
  }

  addUser = (data) => {
    api.put(`api/records/`, JSON.parse(JSON.stringify(data))).then((res) => {
      this.refreshUserTable();
    });
  };

  deleteUser = (id) => {
    api.delete(`api/records/${id}`).then((res) => {
      this.refreshUserTable();
    });
  };

  updateUser = (_id, data) => {
    debugger;

    api
      .post(`api/records/${_id}`, JSON.parse(JSON.stringify(data)))
      .then((res) => {
        this.refreshUserTable();
      });

    this.setEditing(false);
  };

  editRow = (data) => {
    this.setState({
      currentUser: {
        _id: data._id,
        Email: data.data.Email,
        Age: data.data.Age,
        Login: data.data.Login,
      },
    });
    debugger;

    this.setEditing(true);
  };

  setEditing = (isEditing) => {
    this.setState({ editing: isEditing });
  };

  render() {
    const { users } = this.state;

    return (
      <div className="container">
        <div className="row">
          {this.state.editing ? (
            <div className="col s12 l6">
              <h4>Редактирование</h4>
              <EditUserForm
                editing={this.state.editing}
                setEditing={this.setEditing}
                currentUser={this.state.currentUser}
                updateUser={this.updateUser}
              />
            </div>
          ) : (
            <div className="col s12 l6">
              <h4>Добавить пользователя</h4>
              <AddUserForm addUser={this.addUser} />
            </div>
          )}

          <div className="col s12 l6">
            <h5>Пользователи</h5>
            <UserTable
              users={users}
              editRow={this.editRow}
              deleteUser={this.deleteUser}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default Home;
