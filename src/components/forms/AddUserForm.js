import React, { useState } from "react";

const AddUserForm = (props) => {
  const initialFormState = {};
  const [data, setUser] = useState(initialFormState);

  const handleInputChange = (event) => {
    const { name, value } = event.currentTarget;

    setUser({
      ...data,
      [name]: value,
    });
  };

  const submitForm = (event) => {
    event.preventDefault();
    props.addUser({
      data: {
        Email: data.Email,
        Age: data.Age,
        Login: data.Login,
      },
    });

    setUser(initialFormState);
  };

  return (
    <div className="row">
      <form className="col s12" onSubmit={submitForm}>
        <div className="row"></div>

        <div className="row">
          <div className="input-field col s12">
            <input
              type="text"
              name="Email"
              value={data.Email}
              onChange={handleInputChange}
              required
            />
            <label>Email</label>
          </div>
        </div>

        <div className="row">
          <div className="input-field col s12">
            <input
              type="text"
              name="Age"
              value={data.Age}
              onChange={handleInputChange}
              required
            />
            <label>Age</label>
          </div>
        </div>

        <div className="row">
          <div className="input-field col s12">
            <input
              type="text"
              name="Login"
              value={data.Login}
              onChange={handleInputChange}
              required
            />
            <label>Login</label>
          </div>
        </div>

        <div className="row">
          <div className="input-field col s12">
            <button className="waves-effect waves-light btn">Добавить</button>
          </div>
        </div>
      </form>
    </div>
  );
};

export default AddUserForm;
