import React, { useState } from "react";
const EditUserForm = (props) => {
  const [data, setUser] = useState(props.currentUser);

  const handleInputChange = (event) => {
    const { name, value } = event.target;

    setUser({ ...data, [name]: value });
  };

  const submitForm = (event) => {
    event.preventDefault();

    props.updateUser(props.currentUser._id, {
      data: {
        Email: data.Email,
        Age: data.Age,
        Login: data.Login,
      },
    });
  };

  return (
    <div className="row">
      <form className="col s12" onSubmit={submitForm}>
        <div className="row">
          <div className="input-field col s12">
            <input type="text" onChange={handleInputChange} />
            <label>{data._id}</label>
          </div>
          <div className="input-field col s12">
            <input
              type="text"
              name="Email"
              value={data.Email}
              onChange={handleInputChange}
              required
            />
            <label></label>
          </div>
        </div>

        <div className="row">
          <div className="input-field col s12">
            <input
              type="text"
              name="Age"
              value={data.Age}
              onChange={handleInputChange}
              required
            />
            <label></label>
          </div>
        </div>

        <div className="row">
          <div className="input-field col s12">
            <input
              value={data.Login}
              type="text"
              name="Login"
              onChange={handleInputChange}
              required
            />
            <label></label>
          </div>
        </div>

        <div className="row">
          <div className="input-field col s12 m6">
            <button className="waves-effect waves-light btn">Обновить</button>
          </div>

          <div className="input-field col s12 m6">
            <button
              className="waves-effect waves-light btn"
              onClick={() => props.setEditing(false)}
            >
              Отмена
            </button>
          </div>
        </div>
      </form>
    </div>
  );
};

export default EditUserForm;
